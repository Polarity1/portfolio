//vars declareren
var gestopt = 0;
var kaarten = 2;
var somcrd = 0;
var azen = 0;
var htxt = document.getElementById("holdtxt");

//unload functie (initializeren)
function unl() {

    //elementen aanroepen vullen en verbergen
    var fot1 = document.getElementById("foto1");
    var fot2 = document.getElementById("foto2");
    document.getElementById("foto3").style.display = 'none';
    document.getElementById("foto4").style.display = 'none';
    document.getElementById("foto5").style.display = 'none';
    document.getElementById("foto6").style.display = 'none';
    document.getElementById("foto7").style.display = 'none';

    //1e 2 kaarten vullen
    fot1.src = rand_card();
    fot2.src = rand_card();

    //laten zien hoeveel je hebt
    document.getElementById("totcard").innerHTML = "Totaal van je kaarten: <b>" + somcrd;

    //initial blackjack
    if (somcrd == 21) {

        alert("BLACK JACK");
        document.getElementById("totcard").innerHTML = "<b>BLACKJACK!!!</b>";
        htxt.innerHTML = "Opnieuw";
        gestopt = 1;

    }
}

//willekeurige kaart
function rand_card() {

    var card;

    //random getal (0-12)
    var st = Math.floor(Math.random() * 12);

    switch (st) {
        case 0:
            card = "images/13c.jpg";
            somcrd += 10;
            break;
        case 1:
            card = "images/ac.jpg";
            if (somcrd < 21) {
                ++azen;
            }
            somcrd += 11;
            break;
        case 2:
            card = "images/2c.jpg";
            somcrd += 2;
            break;
        case 3:
            card = "images/3c.jpg";
            somcrd += 3;
            break;
        case 4:
            card = "images/4c.jpg";
            somcrd += 4;
            break;
        case 5:
            card = "images/5c.jpg";
            somcrd += 5;
            break;
        case 6:
            card = "images/6c.jpg";
            somcrd += 6;
            break;
        case 7:
            card = "images/7c.jpg";
            somcrd += 7;
            break;
        case 8:
            card = "images/8c.jpg";
            somcrd += 8;
            break;
        case 9:
            card = "images/9c.jpg";
            somcrd += 9;
            break;
        case 10:
            card = "images/10c.jpg";
            somcrd += 10;
            break;
        case 11:
            card = "images/11c.jpg";
            somcrd += 10;
            break;
        case 12:
            card = "images/12c.jpg";
            somcrd += 10;
            break;

    }

    if ((somcrd > 21) && (azen >= 1)) {
        --azen;
        somcrd -= 10;
    }

    //kaart src terugkoppelen
    return card;

}

//Hit button
function hit() {

    var chtxt = document.getElementById("totcard");

    //eerst checken of spel gestopt is
    if (gestopt == 0) {

        //kijken of totaal < 21
        if (somcrd < 21) {

            switch (kaarten) {
                case 2:
                    var fot3 = document.getElementById("foto3");
                    document.getElementById("foto3").style.display = 'inline';
                    fot3.src = rand_card();
                    ++kaarten;
                    break;
                case 3:
                    var fot4 = document.getElementById("foto4");
                    document.getElementById("foto4").style.display = 'inline';
                    fot4.src = rand_card();
                    ++kaarten;
                    break;
                case 4:
                    var fot5 = document.getElementById("foto5");
                    document.getElementById("foto5").style.display = 'inline';
                    fot5.src = rand_card();
                    ++kaarten;
                    break;
                case 5:
                    var fot6 = document.getElementById("foto6");
                    document.getElementById("foto6").style.display = 'inline';
                    fot6.src = rand_card();
                    ++kaarten;
                    break;
                case 6:
                    var fot7 = document.getElementById("foto7");
                    document.getElementById("foto7").style.display = 'inline';
                    fot7.src = rand_card();
                    ++kaarten;
                    break;
            }

        }

    }

    //kijken of nu 21 is gehaald, busted of verder kunnen gaan
    if (somcrd == 21) {

        chtxt.innerHTML = "Je hebt <b>21</b>!! Gefeliciteerd!!"
        htxt.innerHTML = "Opnieuw";

    } else if (somcrd > 21) {

        chtxt.innerHTML = "Totaal van je kaarten: <b>BUSTED</b>";
        htxt.innerHTML = "Opnieuw";
        gestopt = 1;

    } else {

        chtxt.innerHTML = "Totaal van je kaarten: <b>" + somcrd;
        gestopt = 0;

    }

}

//hold/opnieuw button
function hold() {

    //kijken of het spel al is gestopt, zo niet, stopt het spel, zo ja, mogelijkheid om nog een potje te doen
    if (gestopt == 0) {
        alert("Spel gestopt \nJe had: " + somcrd) + "\nDe computer had: " + comp;
        gestopt = 1;
        htxt.innerHTML = "Opnieuw";

    } else {
        if (confirm("Nieuw spelletje?")) {
            gestopt = 0;
            kaarten = 2;
            somcrd = 0;
            unl();
            htxt.innerHTML = "Hold!";
        } else {
            gestopt = 1;
        }

    }

}

function computer() {

    var comp = document.getElementById("comcrd");
    var comtot = 0;
    while (comtot < 18) {

        comtot += Math.floor(Math.random() * 11);
        alert(comtot);

    }

    if (comtot > 21) {

        comp.innerHTML = "Computer is busted!";

    } else {

        comp.innerHTML = "Computer heeft: " + comtot;

    }

}
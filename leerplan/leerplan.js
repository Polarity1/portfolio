var leerarr = document.getElementsByClassName("inp");

function unl() {
    for (let x of leerarr) {
        if (localStorage.getItem(x.name) == "true") {
            x.checked = true;
        }
    }
}

function test() {
    for (let x of leerarr) {
        x.addEventListener("click", e => {
            Save(e.target.name, x.checked);
        });
    }
}

function Save(name, value) {
    localStorage.setItem(name, value);
}

test();